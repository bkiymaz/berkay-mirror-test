# Base image
FROM openjdk:8u111-jdk-alpine

ADD target/*.jar maven-docker-project.jar

ENTRYPOINT ["java","-jar","maven-docker-project.jar"]

EXPOSE 8080

